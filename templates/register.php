<?php $this->layout('layout/default'); ?>

	<h1>&nbsp;</h1>
	<h1 class="text-center" id="impression">Inscription</h1>
	<h1>&nbsp;</h1>

	<div class="container-fluid" ng-module="fp" ng-controller="register">
		<div class="col-md-4"></div>

		<div class="col-md-4" id="display" name="form" >
			<form name="inpt" ng-submit="submit()">
				<div class="form-group has-feedback">
					<label id="e" for="usr">Email</label>
					<input required class="form-control" type="email" name="form.e" ng-model="form.e">
				</div>

				<div id="ev" class="form-group has-feedback">
					<label for="usr">Vérification Email:</label>
					<input required class="form-control" type="email" name="form.ev" ng-model="form.ev" data-toggle="popover" data-trigger="manual"
						title="Les emails ne coincide pas">
				</div>

				<div id="np" class="form-group has-feedback">
					<label for="usr">Nom/Prénom</label>
					<input class="form-control" type="text" name="form.np" ng-model="form.np">
				</div>

				<div id="p" class="form-group has-feedback">
					<label for="pwd">Mot de passe:</label>
					<input required id="pass" type="password" class="form-control" name="form.p" ng-model="form.p" data-toggle="popover" data-trigger="manual"
						title="Mot de passe trop court">
				</div>

				<div id="pv" class="form-group has-feedback">
					<label for="pwd">Vérification Mot de passe:</label>
					<input required id="pass" type="password" class="form-control" name="form.pv" ng-model="form.pv" data-toggle="popover" data-trigger="manual"
						title="Les mots de passe ne coincide pas">
				</div>
				<button type="submit" class="btn btn-default">S'inscrire</button>
				<div>&nbsp;</div>
				<div id="feedback"></div>
			</form>
		</div>

		<div class="<col-md-4></col-md-4>"></div>
	</div>

	<script>
		$('[data-toggle="popover"]').popover(); 
	var app = angular.module('fp', []);
	app.controller('register', function($scope,$window,$location,$http) {
		$scope.submit = function(){
			var form = $scope.form;
			form.action="register";
				if(form.e != form.ev){
					$("[data-toggle='popover'][name='form.ev']").popover('show');
					return false;
				}
				
				if(form.p.length < 6){
					$("[data-toggle='popover'][name='form.p']").popover('show');
					return false;
				}
				
				if(form.p != form.pv){
					$("[data-toggle='popover'][name='form.pv']").popover('show');
					return false;
				}
			$("[data-toggle='popover'][name='form.p']").popover('hide');
			$("[data-toggle='popover'][name='form.ev']").popover('hide');
			$("[data-toggle='popover'][name='form.pv']").popover('hide');
			// console.log($scope.form);
			$data = JSON.stringify(form);
			
			$http.post("scripts/user.php",$data).success(
				function(data,status){
					var con = $("#feedback");
					if(data.code<0){
						con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> ' + data.message + ".	</div>");
					}else{
						con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong> ' + data.message + ".	</div>");
						setTimeout( function () { $window.location.href = "/login.php"},3000);
					}
				});
		}
	});
	</script>
