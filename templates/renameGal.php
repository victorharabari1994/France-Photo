<?php $this->layout('layout/default'); ?>


<h1 class="text-center" id="impression">Renommer les galeries</h1>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center" ng-module='rgal'>
			<table style="margin : auto;">
				<tbody>
					<?php foreach($gals as $key=>$val) :?>
					<tr class='row' ng-controller='rennommer' ng-init="id=<?=$key?>">
						<td style="width : 33%">
							<?=$val?>
						</td>
						<td style="width : 33%"> <input type="text" ng-model="newname" /> </td>
						<td style="width : 33%">
							<button ng-click="rename()" class="btn btn-default">renommer</button>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>

		</div>

		<div class="col-md-3"></div>
	</div>
</div>

<script>
	var ppap = angular.module("rgal",[]);
	ppap.controller('rennommer',function($scope,$http,$window){
		$scope.rename = function() {
			data = {action : 'rename', 'id' : $scope.id,'name' : $scope.newname};
			console.log(data);
			$http.post("scripts/renameGalery.php",data).then(function(){window.location.reload();});
		}
	});

</script>