<?php $this->layout('layout/default'); ?>

<h1 class="text-center" id="impression">&nbsp;</h1>
<h1 class="text-center" id="impression">Aperçu panier</h1>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center">
			<a href="#" onclick="window.history.back()">
				<h3> Retour </h3> </a>

		</div>

		<div class="col-md-3"></div>
	</div>
	<div class="row">
		<div class="col-md-1"></div>

		<div id="photocontainer" class="col-md-10 text-center" ng-module="cart">

			<table class="table table-striped table-responsive table-condenced text-center">
				<thead>
					<tr class="text-center">
						<th>Photo</th>
						<th>Format</th>
						<th>Quantité</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($items as $item) :?>
					<tr ng-controller="rr" ng-init="p='<?=$item[0]?>';f='<?=$item[1]?>';q=<?=$item[2]?>">
						<td>
							<img class="img-thumbnail img-responsive" src="getPhoto.php?p=<?=$item[0]?>" style="width:150px;">
						</td>
						<td>
							<?=$item[1]?>
						</td>
						<td>
							<input required type="number" style="width : 50px;" ng-change="update()" min='0' ng-model="q">
						</td>
						<td>
							<span ng-click="dlt()" style="font-size : 30pt;" class="glyphicon glyphicon-remove"></span>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-default">Passer la commande </button>
		</div>

		<div class="col-md-1"></div>
	</div>
</div>

<div class="container text-center" ng-module="modal" ng-controller="order">

	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog" style="margin-top : 70px;">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Passer la commande</h4>
				</div>
				<div class="modal-body" id="console">
					<h3>Voulez-vous vérifier votre commande avant confirmation.</h3>
					
					<form ng-submit="submit()" role="form">
						<div class="form-group">
							<label for="usr">Nom:</label>
							<input required type="text" class="form-control" id="name"  ng-model="cart.name">
						</div>
						
						<div class="form-group">
							<label for="usr">Prénom:</label>
							<input required type="text" class="form-control" id="last_name"   ng-model="cart.last_name">
						</div>
						
						<div class="form-group">
							<label for="usr">Téléphone:</label>
							<input required type="text" class="form-control" id="phone"  ng-model="cart.phone">
						</div>
						
						<div class="form-group">
							<label for="usr">Adresse:</label>
							<input required type="text" class="form-control" id="adress"  ng-model="cart.adress">
						</div>
						
						<div class="form-group">
							<label for="usr">Mail:</label>
							<input required type="email" class="form-control" id="mail"  ng-model="cart.mail">
						</div>
						
						<div class="form-group">
							<label for="payment">Modalité de paiement</label>
							<select required class="form-control" id="payment" ng-model="cart.payment" data-placement="bottom" data-toggle="popover" title="Choisissez une option">
								<option>J'attends votre facture et je veux qu'on m'envoie par courrier mes photos (frais de port 3,50€).</option>
								<option>Je récupère mes photos au magasin.</option>
							</select>
						</div>
						<p>Tous les champs sont obligatoires</p>
						<button type="submit" class="btn btn-default" >Passer la commande</button>
					</form>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
	var app = angular.module('cart', []);
		app.controller('rr', function($scope, $window , $http, $location, $element) {
			console.log($scope.q);
			
			$scope.dlt = function(){
				console.log($($element));
				console.log([$scope.p, $scope.f, $scope.q]);
				$($element).remove();
				$data = {
					"action" : "ri",
					"item" : [$scope.p, $scope.f, $scope.q]
				}
				$http.post("scripts/cart.php", JSON.stringify($data));
				update();
			} 
			
			$scope.update = function(){
				console.log($($element));
				console.log([$scope.p, $scope.f, $scope.q]);
				$data = {
					"action" : "ui",
					"item" : [$scope.p, $scope.f, $scope.q]
				}
				$http.post("scripts/cart.php", JSON.stringify($data));
				update();
			} 
		});
		
		var app2 = angular.module('modal', []);
		app2.controller('order', function($scope, $window , $http, $location, $element) {
			$scope.submit = function () {
				c= $scope.cart;
				
				if(typeof c.payment == 'undefined'){
					$('[data-toggle="popover"]').popover("show");
					setTimeout(function (){$('[data-toggle="popover"]').popover("hide"); }, 3000);
					return;
				}
				
				$('[data-toggle="popover"]').popover("hide"); 
				
				$data = {
					'action': 'submitorder',
					info : {
						name : c.name,
						last_name : c.last_name,
						phone : c.phone,
						adress : c.adress,
						mail : c.mail,
						payment : c.payment
					}
				};
				
				// console.log(JSON.stringify($data));
				con = $("#console");
				$http.post("scripts/order.php", JSON.stringify($data)).success(function(data,status){
						if(data.code<0){
							con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> Erreur sur serveur :'+data.message+' </div>');
						}else{
							con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong>  </div>');
							setTimeout( function () { $window.location.href = "/index.php"},3000);
						}
						update();
					});
			};
		});
</script>