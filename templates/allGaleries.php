<?php $this->layout('layout/default'); ?>


<h1 class="text-center" id="impression">Galerie</h1>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center">
			<table class="table table-striped">
				<thead>
					<tr>
						<th style="width : 30%">Repetoire</th>
						<th style="width : 30%">Nom</th>
						<th style="width : 30%">Clé</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($gals as $gal) :?>
					<tr>
						<td>
							<?=$gal['location']?>
						</td>
						<td>
							<?=$gal['name']?>
						</td>
						<td>
							<?=$gal['gal_key']?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>

		<div class="col-md-3"></div>
	</div>
</div>