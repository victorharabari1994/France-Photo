<div class="container-fluid">
<footer class="container-fluid text-center">
	<a href="#home" title="To Top" id="toTop">
		<span style="font-size: 40pt;" class="glyphicon glyphicon-chevron-up"></span>
	</a>
	<p>France Photo <br> <a href="#" onclick="(function(){$('#myModal').modal();})()" title="France Photo">nous contacter</a></p>
</footer>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" ng-module='email' ng-controller='mailer'>
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<form name="form" ng-submit="transmit()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Envoyer un message</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="usr">Mail:</label>
						<input type="email" required data-ng-model="email" class="form-control" id="usr">
					</div>
					<div class="form-group">
						<label for="comment">Message:</label>
						<textarea class="form-control" data-ng-model="message" rows="5" id="comment"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default">Envoyer</button>
				</div>
			</form>
		</div>

	</div>
</div>

<script>
	//mailto:p.france-photo@laposte.net
	var mail = angular.module('email',[]);
	mail.controller('mailer', function($scope,$http){
		$scope.transmit = function () {
			data = { 'action' : 'message' , 'email' :$scope.email, 'message' : $scope.message };
			msg = JSON.stringify(data);
			console.log(data);
			$http.post('/scripts/email.php',msg).then(function(){$('#myModal').modal('hide');});
		}
	});

</script>