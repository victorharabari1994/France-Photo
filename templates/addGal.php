<?php $this->layout('layout/default'); ?>

	<h1>&nbsp;</h1>
	<h1 class="text-center" id="impression">Accéder à une nouvelle galerie</h1>
	<h1>&nbsp;</h1>

	<div class="container-fluid" ng-module="fp" ng-controller="key">
		<div class="col-md-4"></div>

		<div class="col-md-4 text-center" id="display">
			<form name="inpt" ng-submit="submit()">
				<div class="form-group">
					<label id="e" for="usr">Clé qui vous est donnée par France Photo :</label>
					<input required class="form-control" type="text" name="form.e" ng-model="form.e">
				</div>
				
				<button type="submit" class="btn btn-default">Accéder</button>
				<div>&nbsp;</div>
			</form>
		</div>	
		

		<div class="col-md-4"></div>
	</div>
	<script>
		var con = $("#display");
		var app = angular.module('fp', []);
		app.controller('key', function($scope, $window , $http, $location) {

			$scope.submit = function(){
				$data.key = $scope.form.e;
				$data.action='addgal';

				// $window.location.href = "scripts/user.php";
				$http.post("scripts/addgal.php", JSON.stringify($data)).success(
					function(data,status){
						if(data.code<0){
							con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> Erreur de serveur.	</div>');
						}else{
							con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong> Succes.	</div>');
							setTimeout( function () { $window.location.href = "/index.php"},3000);
						}
					});
				}
			});
	</script>
