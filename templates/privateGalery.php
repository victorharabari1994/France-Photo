<?php $this->layout('layout/default'); ?>

<h1 class="text-center" id="impression">&nbsp;</h1>
<h1 class="text-center text-capitalize" id="impression">Galerie <?=$name?></h1>
<div class="container-fluid" >
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-center">
			<a href="#" onclick="window.history.back()">
				<h3> Retour </h3> </a>

		</div>

		<div class="col-md-3"></div>
	</div>
	<div class="row">
		<div class="col-md-1"></div>

		<div id="photocontainer" class="col-md-10 text-center" ng-module="PhotoList">

			<?php foreach($list as $photo) :?>
			<div class="thumbnail">
			<img ng-controller="SinglePhoto" ng-init="photoname='<?=$photo?>'" ng-click="click()"name="<?=$photo?>" src="getPhoto.php?p=<?=$photo?>" style="width:300px;">
			<h4><?=$photo?></h4>
			</div>
			<?php endforeach ?>

		</div>

		<div class="col-md-1"></div>
	</div>
</div>

<div class="container text-center">

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg" style="margin-top:50px">

			<!-- Modal content-->
			<div class="modal-content" ng-module="modal" ng-controller="view">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Voir photo <span ng-init="nomdephoto = 'Photo'" ng-bind="nomdepohoto"></span>  </h4>
					
					<h5class="modal-title">
					<span class="rotate glyphicon glyphicon-share-alt " onclick="right()"></span>
					Rotation
					<span class="rotate glyphicon glyphicon-share-alt mirror-y" onclick="left()"></span>
					</h5>
					<script>
							var orient =0;
	
					left = function() {
						orient -= 90;
						$("#bigphoto").rotate({ animateTo : orient, duration : 200});
						return false;
					}
					
					right = function() {
						orient += 90;
						$("#bigphoto").rotate({ animateTo : orient, duration: 200});
						return false;
					}
					</script>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8" style="overflow : hidden">
							<img style="margin: auto; margin-top: 30px; margin-bottom : 30px; max-height: 70vh;" class="img-responsive" src="" id="bigphoto">
						</div>
						<div class="col-md-4">
							<table class="table table-striped">
								<tr>
									<td>
										<p>Format 13x18 prix 12€, quantité <input ng-model="a" type="number" min='0' placeholder="0"> </p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Format 18x24 prix 16€, quantité <input ng-model="b" type="number" min='0' placeholder="0"> </p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Format 20x30 prix 17€, quantité <input ng-model="c" type="number" min='0' placeholder="0"> </p>
									</td>
								</tr>
								<tr>
									<td id="bottd">
										<button type="button" ng-click="send()" class="btn btn-default">Commander </button>
										<input id="filename" type="text" class="sr-only" ng-model="d"></input>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script>
		var app = angular.module('PhotoList', []);
		app.controller('SinglePhoto', function($scope, $window , $http, $location, $element) {
			$scope.click = function(){
				$("#bigphoto").attr('src',$element.attr('src'));
				
				var $sc = angular.element($("#filename")).scope();
				
				$sc.$apply(function (){
					$sc.d = $element.attr('name');
					$sc.a =0;
					$sc.b =0;
					$sc.c =0;
					$sc.nomdepohoto = $scope.photoname;
					$(".alert").remove();
				});
				
				$("#myModal").modal("show");
				console.log($element.attr('src'));
			}
		});
		
		var modal = angular.module('modal', []);
		modal.controller('view', function($scope, $window , $http, $location, $element) {
			console.log("HAHAHAH");
			$scope.d = "";
			$scope.send = function(){
				var $data = {
					"action" : "ai",
					"items" : [
						[$scope.d , "13x18", $scope.a > 0 ? $scope.a : 0],
						[$scope.d , "18x24", $scope.b > 0 ? $scope.b : 0],
						[$scope.d , "20x30", $scope.c > 0 ? $scope.c : 0]
						]};
				var con = $("#bottd");
				$http.post("scripts/cart.php", JSON.stringify($data)).success(function(data,status){
						if(data<1){
							con.append( '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Attention!</strong> Erreur sur serveur </div>');
						}else{
							con.append( '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Succes!</strong> La commande a été mise dans le panier </div>');
						}
						update();
					});
				}
		});
		
	</script>