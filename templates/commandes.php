<?php $this->layout('layout/default'); ?>
<h1 class="text-center" id="impression">Les Commandes</h1>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-left">
			<?php foreach($entries as $entry) :?>
			<div class="well">
				<p> <label>Utilisateur</label>
					<?=$entry['user']['name']?> avec <label>email</label>
						<?=$entry['user']['email']?>
				</p>
				<p> <label>Date</label>
					<?=$entry['date']?>
				</p>
				<div class="well">
					<?php foreach($entry['info'] as $ik => $iv) : ?>
					<p> <label><?=$ik?></label> : <?=$iv?> </p>					
					<?php endforeach; ?>
				</div>
				<?php foreach($entry['order'] as $gal => $phos) :?>
				<p><label>Répertoire<label>:
					<?=$gal?>
				</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Numéro photo</th>
							<th>Format</th>
							<th>Quantité</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($phos as $pho): ?>
						<tr>
							<td style="width : 50%">
								<?=$pho[0]?>
							</td>
							<td style="width : 20%">
								<?=$pho[1]?>
							</td>
							<td  style="width : 20%">
								<?=$pho[2]?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php endforeach; ?>

			</div>
			<?php endforeach; ?>


			<ul class="pager">
				<?php if($p>0): ?>
				<li><a href="commandes.php?p=<?=($p-1)?>">Précédent</a></li>
				<?php endif; ?>
				<li><a href="commandes.php?p=<?=($p+1)?>">Suivant</a></li>
			</ul>
		</div>

		<div class="col-md-3"></div>
	</div>
</div>