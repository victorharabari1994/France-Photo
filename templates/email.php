<div class="container-fluid">
	<div class="row">
		<div class="col-md-3"></div>

		<div class="col-md-6 text-left">
			<?php foreach($entries as $entry) :?>
			<div class="well">
				<p> <label>Utilisateur</label>
					<?=$entry['user']['name']?> avec <label>email</label>
						<?=$entry['user']['email']?>
				</p>
				<p> <label>Date</label>
					<?=$entry['date']?>
				</p>
				<div class="well">
					<?php foreach($entry['info'] as $ik => $iv) : ?>
					<p> <label><?=$ik?></label> : <?=$iv?> </p>					
					<?php endforeach; ?>
				</div>
				<?php foreach($entry['order'] as $gal => $phos) :?>
				<p><label>Repertoire<label>:
					<?=$gal?>
				</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Nom Photo</th>
							<th>Format</th>
							<th>Quantité</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($phos as $pho): ?>
						<tr>
							<td style="width : 50%">
								<?=$pho[0]?>
							</td>
							<td style="width : 20%">
								<?=$pho[1]?>
							</td>
							<td  style="width : 20%">
								<?=$pho[2]?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php endforeach; ?>

			</div>
			<?php endforeach; ?>

		<div class="col-md-3"></div>
	</div>
</div>