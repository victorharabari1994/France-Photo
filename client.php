<?php
session_start();
require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/scripts/local.php";
require_once __DIR__."/vharabar/Galery.php";

$g = new Galery();



$templates = new League\Plates\Engine(__DIR__.'/templates/');

if(!array_key_exists("user", $_SESSION)){
	header("location: login.php");
}

$count = $g->estimateConvertionVolume();

$templates = new League\Plates\Engine(__DIR__.'/templates/');

$templates->addData([
	'count'  => $count,
	'var' => getNavText()
	]);

echo $templates->render("client");



?>