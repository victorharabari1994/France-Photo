<?php

// error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");

require_once __DIR__."/DB.php";
require_once __DIR__."/Config.php";



class Email {
    function sendEmail($id){
        $entries = array();
        
        $db = new DB();
        
        $orders = $db->getOrderById($id);
        foreach($orders as $order){
            $data = $order['details'];
            $data = json_decode($data,true);
            $theUser = $db->getUser($order['acc_id'])[0];
            $entry = '';
            $entry['user']=['name'=>$theUser['name'], 'email' => $theUser['email']];
            
            $entry['info']['nom']=$data['info']['name'];
            $entry['info']['prenom ']=$data['info']['last_name'];
            $entry['info']['téléphone']=$data['info']['phone'];
            $entry['info']['adresse']=$data['info']['adress'];
            $entry['info']['mail ']=$data['info']['mail'];
            $entry['info']['Methode de payment']=$data['info']['payment'];
            
            $entry['date']=$order['date'];
            $entry['order'] = array();
            foreach ($data as $key => $value) {
                if($key=="info")
                continue;
                $entry['order'][$key]=$value;
                
            }
            $entries[] = $entry;
        }
        
        $templates = new League\Plates\Engine(__DIR__.'/../templates/');
        
        $templates->addData([
        'entries' => $entries
        ]);
        
        $message =  $templates->render("email");
        
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From:' .$GLOBALS['cfg']['expeditor']. "\r\n";
        mail($GLOBALS['cfg']['admin_email'],"Nouvelle commande",$message,$headers);
        
    }
    
    function sendMessage($email,$message){
        
        $text ="Expediteur: $email\n\n$message";

		$reciever = $GLOBALS['cfg']['admin_email'];
		$title = "Message_FrancePhoto";
        mail($reciever,$title,$text);
		
    }
}

?>