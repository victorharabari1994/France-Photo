<?php
error_reporting(0);
/*
cart structure
array
	|
photoName => array
				|
			format => count

*/

if(!array_key_exists('cart', $_SESSION))
	$_SESSION['cart'] = array();

class Cart{
	

	public function addItem($photo, $format, $count ){
		$cart = $_SESSION['cart'];
		if(!array_key_exists($photo, $cart))
			$cart[$photo] = array();
		
		$c = 0;
		
		if(!array_key_exists($cart[$photo], $format))
			$c = $cart[$photo][$format];
		
		$cart[$photo][$format] = $c + $count;
		
		$_SESSION['cart'] = $cart;
	}	
	
	public function updateItem($photo, $format, $count ){
		$cart = $_SESSION['cart'];
		if(!array_key_exists($photo, $cart))
			$cart[$photo] = array();
		
		$cart[$photo][$format] = $count;
		
		$_SESSION['cart'] = $cart;
	}	
	
	public function removeItem($photo, $format = null){
		$cart = $_SESSION['cart'];
		if(!array_key_exists($photo, $cart))
			$cart[$photo] = array();
		
		if($format == null)
			unset($cart[$photo]);
		else if(array_key_exists($photo,$cart)){
			unset($cart[$photo][$format]);
		}
		
		$_SESSION['cart'] = $cart;
	}
	
	public function getCart(){
		$cart = $_SESSION['cart'];
		$result = array();
		
		foreach ($cart as $photo => $options) 
			foreach ($options as $format => $count) {
				$result[] = [$photo,$format,$count];
			}
			
		return $result;
	}
	
	public function getCartCount(){
		$c= 0;
		
		$cart = $_SESSION['cart'];
		$result = array();
		
		foreach ($cart as $photo => $options) 
			foreach ($options as $format => $count) {
				$c += $count;
			}
			return $c;
	}
	
}

?>