<?php
error_reporting(0);
require_once __DIR__."/DB.php";

class User{

	public $db;
	
	public function __construct(){
		
		$this->db = new DB();
		
	}
	
	public function init(){
		
		if(array_key_exists('user',$_SESSION) && $_SESSION['user']!=null )
		return;
		
		if(array_key_exists('token',$_COOKIE)){
			
			$user = $this->db->getUser($_COOKIE['token']);
			
			if($user != null && count($user) == 1){
				
				$user = $user[0];
				
				$_SESSION['user']['id'] = $user['id'];
				$_SESSION['user']['name'] = $user['name'];
				$_SESSION['user']['email'] = $user['email'];
				
				
				$res = $this->db->getAdmin($user['id']);
				if($res != null and count($res) > 0)
					$_SESSION['admin']=true;
			}
			
		}
		
	}
	
	public function hashPassword($password){
		
		$salt = uniqid('', true);
		
		$salt = '$2y$10$' . substr(base64_encode($salt),0,22);
		
		$password_hash = crypt($password, $salt);
		
		return $password_hash;
		
	}
	
	public function generateToken(){
		
		$valid = false;
		
		do{
			
			$token = uniqid('', true);
			
			$token = hash('SHA256',$token);
			
			$res = $this->db->getUser($token);
			
			if(count($res) < 1)
			$valid = true;
			
		}
		
		while(!$valid);
		
		return $token;
		
	}
	
	public function validatePassword($password, $pw_hash){
		
		$hash_res = crypt($password,$pw_hash);
		
		return strcmp($hash_res,$pw_hash);
		
	}
	
	public function registerUser($email, $name, $password){
		
		$result = ['code'=>-1, 'message'=>'Erreur serveur'];
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
			$result['message']='Email invalide';
			
			return $result;
			
		}
		
		$res = $this->db->getUser($email);
		
		if($res !=null and count(res)>0){
				$result['code']=-2;
				$result['message']='Email déjà enregistrée';
				return $result;
				
			}
			
		if(strlen($password)<6){
			$result['code']=-3;
			$result['message']='Mot de passe trop court';
			return $result;
		}
			
		$pw_hash = $this->hashPassword($password);
		
		$token = $this->generateToken();
		
		$id = $this->db->addUser($email, $pw_hash, $name, $token);
		
		$result['code']=0;
		$result['message']='Registation réussie';
		return $result;
		
	}
	
	
	
	public function loginUser($email, $password, $remember = false){
		
		$result = ['code'=>-1, 'message'=>'Erreur serveur'];
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
			$result['message']='Email invalide';

			return $result;
			
		}
		
		if(strlen($password)<6){
			$result['code']=-3;
			$result['message']='Mot de passe trop court';
			return $result;
		}
		
		$user = $this->db->getUser($email);
		
		if($user == null || count($user) < 1){
			
			$result['code']=-2;
			
			$result['message']='Email non registrée';
			
			return $result;
		
		}
		
		$user = $user[0];
		
		if(!$this->validatePassword($password, $user['pw_hash'])){
			
			$result['code']=-3;
			
			$result['message']='Mot de passe invalide';
			
			return $result;
			
		}
		
		if($remember){
			$token = $this->generateToken();
			$user['token'] = $token;
			$this->db->updateToken($user['id'],$user['token']);
			setcookie("token", $user['token'], time()+2592000);
		}
		
		$_SESSION['user'] = $user;
		
		$res = $this->db->getAdmin($user['id']);
		if($res != null and count($res) > 0)
		$_SESSION['admin']=true;

		$result['code']=0;
		$result['message']='connexion réussie';
		return $result;
	}
	
	public function updateName($name, $password){
		$id = $_SESSION['user']['id'];
		$result =array();
		
		$lres = $this->loginUser($_SESSION['user']['email'],password);
		if($lres['code'] != 0){
			$result['code']=-1;
			$result['message']='Mot de passe invalide';
			return $result;
		}
		
		$this->db->updateUserName($id,$name);
		$_SESSION['user']['name'] = $name;
		
	}
	
	public function updateEmail($email, $password){
		$id = $_SESSION['user']['id'];
		$result =array();
		
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
			$result['message']='Email invalide';
			
			return $result;
			
		}
		
		$res = $this->db->getUser($email);
		
		if($res !=null and count(res)>0){
				$result['code']=-2;
				$result['message']='Email déjà enregistrée';
				return $result;
				
			}
		
		$lres = $this->loginUser($_SESSION['user']['email'],password);
		if($lres['code'] != 0){
			$result['code']=-1;
			$result['message']='Mot de passe invalide';
			return $result;
		}
		
		$this->db->updateUserEmail($id,$email);
		$_SESSION['user']['email'] = $email;
	}
	
	
	public function updatePassword($oldPassword, $newPassword){
		$id = $_SESSION['user']['id'];
		$result =array();
		
		$lres = $this->loginUser($_SESSION['user']['email'],password);
		if($lres['code'] != 0){
			$result['code']=-1;
			$result['message']='Mot de passe invalide';
			return $result;
		}
		
		$newHash = $this->hashPassword($newPassword);
		
		$this->db->updateUserPassword($id,$newHash);
		
	}
	
	public function logoutUser(){
		// Initialize the session.
		// If you are using session_name("something"), don't forget it now!
		session_start();

		// Unset all of the session variables.
		$_SESSION = array();

		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		// Finally, destroy the session.
		session_destroy();
	}
	
}

?>