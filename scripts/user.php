<?php

session_start();
error_reporting(0);
require __DIR__."/../vharabar/User.php";
require __DIR__."/../vharabar/Input.php";

$in = new Input();
$user = new User();

$data = $in->getJson();
$resp['code']=-100;
$resp['message'] = 'Erreur serveur';

if(  $data['action'] == "login" ){
    if(array_key_exists('user',$_SESSION)){
        $resp['code']=0;
        $resp['message'] ="Vous-etes déjà enregistrée";
        $in->sendJson($resp);
        return;
    }
    
    $resp = $user->loginUser($data['e'],$data['p'],$data['r']);
    $in->sendJson($resp);
    return;
}
if($data['action'] == 'register'){
    if(array_key_exists('user',$_SESSION)){
        $resp['code']=0;
        $resp['message'] ="Vous-etes déjà enregistrée";
        $in->sendJson($resp);
        return;
    }
    
    if($data['ev'] != $data['e']){
        $resp['code']=-1;
        $resp['message'] = 'Les emails ne coincident pas';
        $in->sendJson($resp);
        return;
    }
    
    if($data['pv'] != $data['p']){
        $resp['code']=-2;
        $resp['message'] = 'Les mots de passe ne coïncident pas';
        $in->sendJson($resp);
        return;
    }
    
    $resp = $user->registerUser($data['e'],$data['np'],$data['p']);
    $in->sendJson($resp);
    return;
}


if(array_key_exists('user',$_SESSION))
if(  $data['action'] == "update" ){
    if(array_key_exists('email', $data) and $data['email'] != ""){
        $resp = $user->updateEmail($data['email'], $data['pass']);
        if($resp['code'] < 0){
            $in->sendJson($resp);
            return;
        }
        
    }
    if(array_key_exists('name', $data) and $data['name'] != ""){
        $resp = $user->updateName($data['name'], $data['pass']);
        if($resp['code'] < 0){
            $in->sendJson($resp);
            return;
        }
        
    }
    
    if(array_key_exists('pass', $data)  and $data['pass'] != "")
		if($data['new_pass'] == $data['new_pass_ver']){
			$resp = $user->updatePassword($data['pass'], $data['new_pass']);
			if($resp['code'] < 0){
				$in->sendJson($resp);
				return;
			}
		}else{
			$resp['code']=-1;
			$resp['message']="les mots de passe ne coincident pas";
			$in->sendJson($resp);
			return;
		}
	$resp['code']=0;
	$resp['message']="Aucune erreur";
    $in->sendJson($resp);
    return;
}

$in->sendJson($resp);
?>