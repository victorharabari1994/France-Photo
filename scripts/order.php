<?php
session_start();

require __DIR__."/../vharabar/Order.php";
require __DIR__."/../vharabar/Input.php";


$in = new Input();


if(!array_key_exists('user',$_SESSION)){
	$in->sendJson(null);
	return;
}


$data = $in->getJson();
$action = $data['action'];

if($action == "submitorder"){
	
	$o = new Order();
	
	$in->sendJson($o->processOrder($data['info']));
	unset($_SESSION['cart']);
	return;
}


	$in->sendJson(0);
return ;


?>