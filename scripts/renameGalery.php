<?php
session_start();

require __DIR__."/../vharabar/Galery.php";
require __DIR__."/../vharabar/Input.php";

$in = new Input();
$g = new Galery();


if(!array_key_exists('user',$_SESSION)){
	$in->sendJson(null);
	return;
}

$data = $in->getJson();
if($data['action'] == "rename"){
	$g->updateUserGaleryName($_SESSION['user']['id'],$data['id'],$data['name'], $_SESSION['admin']);
}

?>