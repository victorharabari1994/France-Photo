<?php
session_start();

if(!array_key_exists("user", $_SESSION)){
	header("location: login.php");
}


require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Cart.php";
require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$c = new Cart();

$list = $c->getCart();

$templates->addData([
	'items' => $list,
	'var' => getNavText()
]);

echo $templates->render("cart");


?>