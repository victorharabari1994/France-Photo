<?php
session_start();

// if(!array_key_exists('admin',$_SESSION)){
//     return;
// }

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/scripts/local.php";
require_once __DIR__."/vharabar/Galery.php";
require_once __DIR__."/vharabar/User.php";
require_once __DIR__."/vharabar/DB.php";

$db = new DB();


$entries = array();
/*entry
	|
  'user' => array
    |         |
	|       'name' => name
	|         |
	|       'email' => email
	|
  'info' => info
  	|
  'date' => date
    |
 'order' => array
 			  |
			galeryName => array
							|
						'0' => photoname
							|
						'1' => format
							|
						'2' => count

*/
//[id, accid , date, data]
//data = [info = "userinfo", #galName => [photoname,format,quant] ]

$p=0;
if(array_key_exists('p',$_GET))
	$p = $_GET['p'];
	
$off = $p*10;

$orders = $db->getAllOrders($off);
foreach($orders as $order){
	$data = $order['details'];
	$data = json_decode($data,true);
	$theUser = $db->getUser($order['acc_id'])[0];
	$entry = '';
	$entry['user']=['name'=>$theUser['name'], 'email' => $theUser['email']];
	
	$entry['info']['Nom']=$data['info']['name'];
	$entry['info']['Prénom ']=$data['info']['last_name'];
	$entry['info']['Téléphone']=$data['info']['phone'];
	$entry['info']['Adresse']=$data['info']['adress'];
	$entry['info']['Mail ']=$data['info']['mail'];
	$entry['info']['Modalité de paiement']=$data['info']['payment'];
	
	$entry['date']=$order['date'];
	$entry['order'] = array();
	foreach ($data as $key => $value) {
		if($key=="info")
			continue;
			$entry['order'][$key]=$value;
		
	}
	$entries[] = $entry;
}

$templates = new League\Plates\Engine(__DIR__.'/templates/');


$templates->addData([
	'entries' => $entries,
	'var' => getNavText(),
	'p' => $p
	]);

echo $templates->render("commandes");


?>