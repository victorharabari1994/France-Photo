<?php
session_start();

if(!array_key_exists("user", $_SESSION)){
	header("location: login.php");
}


require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";

require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$g = new Galery();

$dir = $g->getUsersGaleries($_SESSION['user']['id']);
$list = array();

if(isset($dir))
foreach ($dir as $key => $value) {
	$phot = $g->getGalleryPhotoList($_SESSION['user']['id'],$key,$_SESSION['admin']);
	$list[$key][1] = array_pop($phot); 
	$t = array_pop($phot);
	$list[$key][1] = $t != null ? $t : $list[$key];
	$match = '';
	$list[$key][0] = $value[1];
}
	
$templates->addData([
	'gals' => $list,
	'var' => getNavText()
]);

echo $templates->render("galeryp");


?>