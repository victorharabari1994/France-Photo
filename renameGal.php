<?php
session_start();

if(!array_key_exists("user", $_SESSION)){
	header("location: login.php");
}


require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/Galery.php";
require_once __DIR__."/scripts/local.php";


$templates = new League\Plates\Engine(__DIR__.'/templates/');

$g = new Galery();
// $dir = $g->getAllGaleries(true);
$dir = $g->getAllGaleries( $_SESSION['user']['id'],$_SESSION['admin']);
$list = array();

if(isset($dir))
foreach ($dir as $value) {
	$list[$value['id']]  = $value['name'];
}
	
$templates->addData([
	'gals' => $list,
	'var' => getNavText()
]);

echo $templates->render("renameGal");


?>