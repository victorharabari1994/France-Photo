<?php

try{
session_start();

require_once __DIR__."/vendor/autoload.php";
require_once __DIR__."/vharabar/User.php";
require_once __DIR__."/scripts/local.php";




$u = new User();
$u->init();
$d = $_SESSION;
$templates = new League\Plates\Engine(__DIR__.'/templates/');
$d = getNavText();

$templates->addData([
	'links' => [
		"#home" => "<span style='font-size: 11pt;' class='glyphicon glyphicon-upload'></span>",
		'#impression' => 'Impression sur place',
		'#cabine' => "Cabine photo",
		'#partners' => "Partenaires",
		'#contacts' =>'Contacts'
	],
	'var' => getNavText()
	]);

echo $templates->render("index");
}
catch(Exception $e){
	print $e->getMessage();
}
?>